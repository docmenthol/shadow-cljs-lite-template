(defproject shadow-cljs-lite/lein-template "0.1.0"
  :description "Preconfigured Shadow CLJS template."
  :url "https://gitlab.com/sli/shadow-cljs-lite-template"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories
  {"clojars" {:url "https://clojars.org/repo"
              :sign-releases false}}
  :eval-in-leiningen true)
