(ns leiningen.new.shadow-cljs-lite
  (:require [leiningen.new.templates :refer [renderer raw-resourcer name-to-path ->files]]
            [leiningen.core.main :as main]))

(def render (renderer "shadow-cljs-lite"))
(def raw (raw-resourcer "shadow-cljs-lite"))

(defn in?
  [coll elm]
  (some #(= elm %) coll))

(defn reframe-files
  [data]
  [["src/{{sanitized}}/db.cljs" (render "src/db.cljs" data)]
   ["src/{{sanitized}}/events.cljs" (render "src/events.cljs" data)]
   ["src/{{sanitized}}/subs.cljs" (render "src/subs.cljs" data)]])

(defn tailwind-files
  [data]
  [["resources/css/styles.css" (raw "resources/tailwind-styles.css")]
   ["postcss.config.js" (render "postcss.config.js" data)]])

(defn root-files
  [data]
  [["package.json" (render "package.json" data)]
   ["shadow-cljs.edn" (render "shadow-cljs.edn" data)]
   ["README.md" (raw "README.md")]
   [".gitignore" (raw ".gitignore")]])

(defn resource-files
  [data]
  [["resources/public/css/styles.css" (raw "resources/styles.css")]
   ["resources/public/index.html" (render "resources/index.html" data)]])

(defn source-files
  [data]
  [["src/{{sanitized}}/core.cljs" (render "src/core.cljs" data)]
   ["src/{{sanitized}}/dev.cljs" (render "src/dev.cljs" data)]
   ["src/{{sanitized}}/views/core.cljs" (render "src/views/core.cljs" data)]])

(defn templates-by-lib
  [data]
  (cond->>
    []
    (:reframe data)  (concat (reframe-files data))
    (:tailwind data) (concat (tailwind-files data))))

(defn shadow-cljs-lite
  [name & lib]
  (let [reframe (in? lib "+reframe")
        tailwind (in? lib "+tailwind")
        data {:name name
              :sanitized (name-to-path name)
              :reframe reframe
              :tailwind tailwind}]
    (main/info (apply str ["Generating empty shadow-cljs + Reagent" (when reframe " + Re-frame") (when tailwind " + Tailwind") " project."]))
    (->> (root-files data)
         (concat (resource-files data))
         (concat (source-files data))
         (concat (templates-by-lib data))
         (apply ->files data))))
