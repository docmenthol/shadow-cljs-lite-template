# shadow-cljs-lite

Preconfigured [shadow-cljs](https://github.com/thheller/shadow-cljs) template.

## Supports

* Reagent
* Re-frame
* Tailwind CSS

A browser repl can be accessed quickly with `yarn repl`.

Re-frame comes with an empty state with spec, `:initialize` event handler, an
empty namespace for subscriptions, and `re-frame-10x` debug tools.

Tailwind CSS comes with `concurrently` and PostCSS, and `yarn start` is given
the power to hot reload CSS alongside application code. Adds new `yarn watch-*`
tasks.

## Usage

``` shell
lein new shadow-cljs-lite your-project
lein new shadow-cljs-lite your-project +reframe
lein new shadow-cljs-lite your-project +tailwind
lein new shadow-cljs-lite your-project +reframe +tailwind
```

## TODO

* Add Om and Rum support.
* Add Tachyons CSS support.

## License

Copyright © 2019 sli

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
