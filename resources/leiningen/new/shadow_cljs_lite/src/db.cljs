(ns {{name}}.db
  (:require [cljs.spec.alpha :as s]))

(s/def ::db map?)

(def initial-state {})
