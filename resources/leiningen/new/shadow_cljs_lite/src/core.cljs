(ns {{name}}.core
  (:require [reagent.core :as r]
            {{^reframe}}
            [{{name}}.views.core :refer [app]]))
            {{/reframe}}
            {{#reframe}}
            [{{name}}.views.core :refer [app]]
            [re-frame.core :refer [dispatch-sync]]
            [{{name}}.events]))
            {{/reframe}}

(defn start []
  (r/render-component
    [app]
    (. js/document (getElementById "app"))))

(defn ^:export init
  []
  {{#reframe}}
  (dispatch-sync [:initialize])
  {{/reframe}}
  (start))

(defn stop
  []
  (js/console.log "stop"))
