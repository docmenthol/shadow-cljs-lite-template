(ns {{name}}.events
  (:require [re-frame.core :as rf]
            [cljs.spec.alpha :as s]
            [{{name}}.db :refer [initial-state]]))

(defn check-and-throw
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

(def check-spec-interceptor
  (rf/after (partial check-and-throw :{{name}}.db/db)))

(rf/reg-event-db
  :initialize
  [check-spec-interceptor]
  (fn [_ _] initial-state))
