# {{name}}

## Run

``` shell
yarn install
yarn start
```

## Clean

``` shell
yarn clean
```

## Build

``` shell
yarn build
```

## License

Copyright © 2019 me

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
